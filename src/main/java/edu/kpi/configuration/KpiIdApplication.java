package edu.kpi.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@ComponentScan(basePackages = "edu.kpi")
@EnableJpaRepositories("edu.kpi.repository")
public class KpiIdApplication {

	public static void main(String[] args) {
		SpringApplication.run(KpiIdApplication.class, args);
	}
}
