package edu.kpi.configuration;

import com.fasterxml.jackson.annotation.ObjectIdResolver;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.databind.introspect.Annotated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.http.converter.json.SpringHandlerInstantiator;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Configuration
@EnableWebMvc
public class WebMvcConfiguration  extends WebMvcConfigurerAdapter {
    @PersistenceContext
    private EntityManager em;
    @Autowired
    private ApplicationContext applicationContext;


    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setViewClass(JstlView.class);
        resolver.setPrefix("/WEB-INF/view/");
        resolver.setSuffix(".jsp");
        return resolver;
    }

   /* @Bean
    public Jackson2ObjectMapperBuilder configureObjectMapper() {
        Jackson2ObjectMapperBuilder mapBuilder = new Jackson2ObjectMapperBuilder()
                .modulesToInstall(Hibernate5Module.class).handlerInstantiator(
                        new SpringHandlerInstantiator(this.applicationContext.getAutowireCapableBeanFactory()) {
                            @Override
                            public ObjectIdResolver resolverIdGeneratorInstance(
                                    final MapperConfig<?> config,
                                    final Annotated annotated,
                                    final Class<?> implClass) {
                                if (implClass == EntityIdResolver.class) {
                                    return new EntityIdResolver(em);
                                }
                                return null;
                            }
                        });
        return mapBuilder;
    }*/

}
