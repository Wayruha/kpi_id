package edu.kpi.controller;

import edu.kpi.model.Faculty;
import edu.kpi.repository.FacultyRep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Controller
public class MainController {

    @PersistenceContext
    EntityManager entityManager;
    @Autowired
    FacultyRep facultyRep;
    @RequestMapping("/*")
    public ModelAndView mainHandler(){
        ModelAndView mav = new ModelAndView("index");

        Faculty fac = new Faculty();
        fac.setLongName("Faculty ICT");
        fac.setShortName("FICT");
        fac = facultyRep.save(fac);

        mav.addObject("faculty", fac);
        return mav;
    }
}
