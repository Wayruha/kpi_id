package edu.kpi.dto;

public class StudentAdditionalInf {
    private final int[] numberIndexes;
    private final int[] numberValues;

    public StudentAdditionalInf(int[] numberIndexes, int[] numberValues) {
        this.numberIndexes = numberIndexes;
        this.numberValues = numberValues;
    }

    public int[] getNumberIndexes() {
        return numberIndexes;
    }

    public int[] getNumberValues() {
        return numberValues;
    }
}
