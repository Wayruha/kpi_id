package edu.kpi.dto;

import java.util.Date;

public class StudentBasicInf {
    private final String name;
    private final String surname;
    private final String fathersName;
    private final String faculty;
    private final String group;
    private final Date birthdate;
    private final String idCardNumber;

    public StudentBasicInf(String name, String surname, String fathersName, String faculty, String group, Date birthdate, String idCardNumber) {
        this.name = name;
        this.surname = surname;
        this.fathersName = fathersName;
        this.faculty = faculty;
        this.group = group;
        this.birthdate = birthdate;
        this.idCardNumber = idCardNumber;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getFaculty() {
        return faculty;
    }

    public String getGroup() {
        return group;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getIdCardNumber() {
        return idCardNumber;
    }
}
