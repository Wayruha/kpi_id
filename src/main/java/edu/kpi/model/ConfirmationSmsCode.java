package edu.kpi.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "\"CNFRM_SMS_CODE\"")
public class ConfirmationSmsCode {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long codeId;
    @Column
    private String phoneNumber;
    @Column
    private String code;
    @Column
    private Boolean active;
    @Column
    private Date expireAt;

    public ConfirmationSmsCode() {
    }

    public long getCodeId() {
        return codeId;
    }

    public void setCodeId(long codeId) {
        this.codeId = codeId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Date getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Date expireAt) {
        this.expireAt = expireAt;
    }
}
