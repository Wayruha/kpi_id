package edu.kpi.model;

import javax.persistence.*;

@Entity
@Table(name = "\"KPI_ID\"")
public class KPIID {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long kpiId;
    @Column
    private Long universityId;
    @Column
    private String phoneNumber;
    @ManyToOne
    @JoinColumn(name = "facultyId")
    private Faculty faculty;
    @Column(name="\"group\"")
    private String group;
    @Column
    private Boolean active;
    @Column
    private Boolean isStudying;

    public KPIID() {
    }

    public Long getKpiId() {
        return kpiId;
    }

    public void setKpiId(Long kpiId) {
        this.kpiId = kpiId;
    }

    public Long getUniversityId() {
        return universityId;
    }

    public void setUniversityId(Long universityId) {
        this.universityId = universityId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Faculty getFaculty() {
        return faculty;
    }

    public void setFaculty(Faculty faculty) {
        this.faculty = faculty;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getStudying() {
        return isStudying;
    }

    public void setStudying(Boolean studying) {
        isStudying = studying;
    }
}
