package edu.kpi.repository;

import edu.kpi.model.ConfirmationSmsCode;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ConfirmationCodeRep extends CrudRepository<ConfirmationSmsCode, Long> {

    List<ConfirmationSmsCode> findAll();
}
