package edu.kpi.repository;

import edu.kpi.model.Faculty;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FacultyRep extends CrudRepository<Faculty, Long> {
    List<Faculty> findAll();
}
