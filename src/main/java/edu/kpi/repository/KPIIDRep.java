package edu.kpi.repository;

import edu.kpi.model.ConfirmationSmsCode;
import edu.kpi.model.KPIID;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KPIIDRep extends CrudRepository<KPIID, Long> {

    List<KPIID> findAll();
}
