package edu.kpi.service;

import edu.kpi.dto.StudentAdditionalInf;
import edu.kpi.dto.StudentBasicInf;

public interface AcademicVerifierService {
    /**
     *
     * @param studentBasicInf basic student information (name, age, faculty)
     * @return student academic ID (ID in their DB)
     */
    String verifyStudent(StudentBasicInf studentBasicInf);

    boolean additionalStudentVerifyingStep(String academicId, StudentAdditionalInf studentAdditionalInf);

}
