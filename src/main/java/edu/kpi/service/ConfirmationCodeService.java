package edu.kpi.service;

public interface ConfirmationCodeService {
    void registerNewCode(String phoneNumber);
    boolean confirmCode(String phoneNumber, String code);
}
