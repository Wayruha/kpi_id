package edu.kpi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;


@SpringBootApplication
@ComponentScan(value = "edu.kpi.repository")
@EntityScan(value = "edu.kpi.model")
public class KpiIdApplicationTests {

    public static void main(String[] args) {
        System.out.println("In main of TestConfiguration");
        ApplicationContext ctx = SpringApplication.run(KpiIdApplicationTests.class, args);
        String[] beanNames = ctx.getBeanDefinitionNames();
        Arrays.sort(beanNames);

        System.out.println("Spring boot test generated " + beanNames.length + " beans");

    }

}
