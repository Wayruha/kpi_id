package edu.kpi.repository;

import edu.kpi.model.Faculty;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@Rollback
public class FacultyRepositoryTest {
    @Autowired
    FacultyRep facultyRep;

    @Before
    public void setUp() throws Exception {
        System.out.println("---starting Faculty repository test -----");
    }


    @Test
    public void addEntity() {
        Faculty fac = new Faculty();
        fac.setShortName("testLg");
        fac.setLongName("long name test");
        facultyRep.save(fac);

        Faculty fac2 = facultyRep.findAll().get(0);
        Assert.assertEquals(fac.getFacultyId(), fac2.getFacultyId());
    }

    @Test
    public void addEntityTwoTime() {
        Faculty fac = new Faculty();
        fac.setShortName("IEE");
        fac.setLongName("lInstitaEE");
        facultyRep.save(fac);
        Faculty fac2 = new Faculty();
        fac.setShortName("FICT");
        fac.setLongName("FICTLONG");
        facultyRep.save(fac2);

        Assert.assertEquals(facultyRep.findAll().size(),2);
    }


}
